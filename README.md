# @allbin/express-jwt

Express middleware for verification of JWTs.

## Install

```bash
$ npm install @allbin/express-jwt
```

## Usage

```js
import jwt from '@allbin/express-jwt'

jwt.createVerificationKey('my-secret', 'abcabc123123');
jwt.createVerificationKeyFromFile('my-pubkey', '/pubkey.pem');

app.get('/protected/secret', jwt.verify({key: "my-secret"}), (req, res, next) => { /* ... */ }
app.get('/protected/pubkey', jwt.verify({key: "my-pubkey"}), (req, res, next) => { /* ... */ }
```

## Functions

### jwt.createVerificationKey(id, data)

Creates a reference identifier for a given verification key. `data` must be a string/Buffer containing either a raw secret or a PEM public key.

### jwt.createVerificationKeyFromFile(id, path)

Same as above but the key is read from a file located at `path`.

### jwt.verify(options)

Creates a middleware that verifies incoming JWTs.

Options:  
`key` - (required) identifier for verification key to use for signature verification  
`rejectUnauthorizedRequests` - if set, unauthorized requests will be rejected with HTTP 401/403. Defaults to: `true`.

Other options are passed on to `jsonwebtoken.verify`.

On successful JWT verfication, the middleware will set `req.user` (decoded token payload) and `req.raw_token` (raw token string).
