import jwt from 'jsonwebtoken';
import fs from 'fs';

const readFile = path => {
    try {
        return fs.readFileSync(path);
    } catch (err) {
        throw new Error(`Unable to read file: ${path}`);
    }
};

const createMiddleware = (secretOrPubKey, options) => {
    return (req, res, net) => {
        const [scheme, raw_token] =
            (req.headers.authorization &&
                req.headers.authorization.split(' ')) ||
            [];

        if (scheme !== 'Bearer') {
            res.header('WWW-Authenticate', 'Bearer');
            res.status(401).json({ message: 'Unauthorized' });
            return;
        }

        jwt.verify(raw_token, secretOrPubKey, options, (err, decoded) => {
            if (err) {
                res.status(403).json({ message: 'Invalid token', error: err });
            } else {
                req.user = decoded;
                req.raw_token = raw_token;
                next();
            }
        });
    };
};

let keys = {};

const ExpressJwt = {
    createVerificationKey: (id, data) => {
        keys[id] = data;
    },

    createVerificationKeyFromFile: (id, path) => {
        keys[id] = readFile(path);
    },

    verify: opts => {
        const opts_default = {
            rejectUnauthorizedRequests: true,
        };

        let options = Object.assign({}, opts_default, opts);
        if (!options.hasOwnProperty('key')) {
            throw new Error(
                'No JWT verification key selected for verification',
            );
        }

        const id = options.key;
        const key = keys[id];
        if (!key) {
            throw new Error(
                `Invalid JWT verification key selected for verification: ${
                    opts.key
                }`,
            );
        }

        return createMiddleware(key, options);
    },
};

export { ExpressJwt as jwt };
